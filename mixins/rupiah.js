export default {
  methods: {
    toRupiah(x) {
      if (x) {
        if (x != "" && x != 0) {
          return "Rp " + x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        } else {
          return "Rp. 0";
        }
      } else {
        return "Rp. 0";
      }
    },
  },
  mounted() {
    console.log("mixin loaded");
  },
};
